/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.repository;

import br.com.spring.configuration.AbstractRepository;
import br.com.spring.model.Person;
import br.com.spring.model.PersonStatus;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author fatecie26
 */
public interface PersonRepository extends AbstractRepository<Person, Long>{
    
    @Query(value = "from Person as person where person.city = :city and person.status = :status")
    public List<Person> findPersonsCity(@Param("city") String city, @Param("status") PersonStatus status);
    
}
