/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.repository;

import br.com.spring.configuration.AbstractRepository;
import br.com.spring.model.Admin;
import java.io.Serializable;

/**
 *
 * @author fatecie26
 */
public interface AdminRepository extends AbstractRepository<Admin, Long>{
    
    public Admin findByEmailAndPassword(String email, String password);
    
}
