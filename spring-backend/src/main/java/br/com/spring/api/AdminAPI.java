/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.api;

import br.com.spring.model.Admin;
import br.com.spring.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fatecie26
 */
@RestController
@RequestMapping(value = "/api/admin")
public class AdminAPI {
    
    @Autowired
    private AdminService service;
    
    
    @RequestMapping(value = "/login/{email}/{password}", method = RequestMethod.GET)
    public Admin login(@PathVariable("email") String email, @PathVariable("password") String password){
        return service.findAdmin(email, password);
    }
    
}
