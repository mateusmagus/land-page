/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.api;

import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Admin;
import br.com.spring.model.Person;
import br.com.spring.service.AdminService;
import br.com.spring.service.PersonService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fatecie26
 */
@RestController
@RequestMapping(value = "/api/person")
public class PersonAPI {
    
    @Autowired
    private PersonService service;
    
    /**
     *
     * @param message to all persons
     * @return
     */
    @RequestMapping(value = "/send-all", method = RequestMethod.POST)
    public RestResponse sendEmailAllPersons(@RequestBody MessageDTO message){
        return service.sendEmailMultiplePersons(message.getSubject(), message.getMessage());
    }
    
    /**
     *
     * @param message to selected persons
     * @return
     */
    @RequestMapping(value = "/send-persons", method = RequestMethod.POST)
    public RestResponse sendEmailToPersons(@RequestBody MessageDTO message){
        return service.sendEmailMultiplePersons(message.getPersons(), message.getSubject(), message.getMessage());
    }
    
    
    @RequestMapping(value = "/city/{city}", method = RequestMethod.GET)
    public List<Person> findPersonsCity(@PathVariable("city") String city){
        return service.findPersonsCity(city);
    }
    
    @RequestMapping(value = "/inactive", method = RequestMethod.PUT)
    public Person inactivePerson(@RequestBody Person person){
        return service.inactivePerson(person);
    }
    
}

class MessageDTO{
    private List<Person> persons;
    String subject;
    String message;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }
    
    
}
