/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.configuration;

/**
 *
 * @author fatecie26
 */
public class RestResponse {
    
    private String message;

    public RestResponse() {
    }

    public RestResponse(String message) {
        this.message = message;
    }

    
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
}
