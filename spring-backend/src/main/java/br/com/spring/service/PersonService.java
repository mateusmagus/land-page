/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.service;

import br.com.spring.configuration.RestResponse;
import br.com.spring.model.Admin;
import br.com.spring.model.Person;
import br.com.spring.model.PersonStatus;
import br.com.spring.repository.AdminRepository;
import br.com.spring.repository.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.ExtensionInstallationException;

/**
 *
 * @author fatecie26
 */
@Service
public class PersonService {

    @Autowired
    private EmailService emailService;

    private PersonRepository repository;

    @Autowired
    private PersonService(PersonRepository repository) {
        this.repository = repository;
    }

    public PersonService() {
    }

    @Transactional
    public Person savePersonAndSendEmail(Person person) {
        try {
            person.setStatus(PersonStatus.ACTIVE);
            repository.save(person);
            sendEmailWelcome(person);
        } catch (Exception exception) {
            System.out.println("Line 39 - ERROR ON SAVE - " + exception.getMessage());
        }
        return person;
    }
    
    public Person inactivePerson(Person person){
        person.setStatus(PersonStatus.INATIVE);
        return repository.save(person);
    }

    public void sendEmailWelcome(Person person) {
        emailService.sendEmail(person, "title", "message");
    }

    public RestResponse sendEmailMultiplePersons(List<Person> persons, String subject, String message) {
        return sendMultiple(persons, subject, message);
    }

    public RestResponse sendMultiple(List<Person> persons, String subject, String message) {
        try {
            emailService.sendEmailMultiplePerson(persons, subject, message);
            return new RestResponse("Erro ao enviar e-mail, contate um administrador!");
        } catch (Exception exception) {
            return new RestResponse("Erro ao enviar e-mail, contate um administrador!");
        }
    }

    public RestResponse sendEmailMultiplePersons(String subject, String message) {
        List<Person> persons = repository.findAll();
        return sendMultiple(persons, subject, message);
    }

    public List<Person> findPersonsCity(String city){
        return repository.findPersonsCity(city, PersonStatus.ACTIVE);
    }
    
}
