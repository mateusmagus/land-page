/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.service;

import br.com.spring.model.Admin;
import br.com.spring.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author fatecie26
 */
@Service
public class AdminService {
    
    
    private AdminRepository repository;
    
    @Autowired
    private AdminService(AdminRepository repository){
        this.repository = repository;
    }
    
    public AdminService(){
    }
    
    
    public Admin findAdmin(String email, String password){
        return repository.findByEmailAndPassword(email, password);
    }
    
}
