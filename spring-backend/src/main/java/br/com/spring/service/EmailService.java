/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.spring.service;

import br.com.spring.model.Person;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Service;

/**
 *
 * @author fatecie26
 */
@Service
public class EmailService {

    public void sendEmailOnePerson(Person person, String subject, String message) {
        sendEmail(person, subject, message);
    }

    public void sendEmailMultiplePerson(List<Person> persons, String subject, String message) {
        for (Person person : persons) {
            sendEmail(person, subject, message);
        }
    }

    public void sendEmail(Person person, String subject, String message) {
        try {
            Email email = new SimpleEmail();
            email.setHostName("smtp.googlemail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator("fatecietrabalho@gmail.com", "fateciesi2016"));
            email.setSSLOnConnect(true);
            email.setFrom("fatecietrabalho@gmail.com");
            email.setSubject(subject);
            email.setMsg(subject);
            email.addTo(person.getEmail());
            email.send();
        } catch (EmailException ex) {
            System.out.println("Line 48 ERROR ON SEND EMAIL : "+ex.getMessage());
        }
    }

}
