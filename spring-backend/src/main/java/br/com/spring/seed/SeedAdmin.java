/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.seed;

import br.com.spring.model.Admin;
import br.com.spring.repository.AdminRepository;
import br.com.spring.repository.PersonRepository;
import java.security.NoSuchAlgorithmException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author fatecie26
 */
@Component
public class SeedAdmin implements AppSeed{

    @Autowired
    private AdminRepository adminRepository;
    
    @Override
    public void loadSeed() throws NoSuchAlgorithmException {
        if(adminRepository.findAll().isEmpty()){
            Admin admin = new Admin();
            admin.setName("Admin Fatecie :)");
            admin.setEmail("fatecietrabalho@gmail.com");
            admin.setPassword("fateciesi2016");
            adminRepository.save(admin);
        }
    }
    
}
