/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.spring.seed;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 *
 * @author fatecie26
 */
@Component
public class Seed  implements ApplicationListener<ContextRefreshedEvent>{

    @Autowired
    private SeedAdmin seedAdmin;
    
    List<AppSeed> seeds = new ArrayList<>();
    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        for(AppSeed seed: seeds()){
            try {
                seed.loadSeed();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Seed.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    public List<AppSeed> seeds(){
        seeds.add(seedAdmin);
        return seeds;
    }
    
    
}
